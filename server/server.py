#!/usr/bin/env python3

# import sys
# sys.path.append("./")

from lib.json2object import json2data
from lib.comServerProgramming import ConnServer2prog

config = json2data('cfg.json')
print(config)

# start_server2Programming(config['host'], config['port_sck'])
conSer2prog = ConnServer2prog(config['host'], config['port_sck'])
conSer2prog.send(b'hola')
